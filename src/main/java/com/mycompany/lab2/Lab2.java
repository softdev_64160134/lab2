/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab2;

import java.util.Scanner;

/**
 *
 * @author USER
 */
public class Lab2 {

    static char[][] board = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private static char currentPlayer = 'X';
    private static int row;
    private static int col;

    public static void main(String[] args) {
        printWelcome();
        while (true) {
            printBoard();
            printTurn();
            inputNumber();

            if (isWinner()) {
                printBoard();
                printWinner();
                break;
            } else if (isDraw()) {
                printBoard();
                printDraw();
                break;
            }
            switchPlayer();
        }

    }

    private static void printWelcome() {
        System.out.println("Welcome to OX !!! ");
    }

    private static void printBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private static void printTurn() {
        System.out.println("Player " + currentPlayer + " turn");
    }

    private static void inputNumber() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Enter a number to place: ");
            row = sc.nextInt();
            col = sc.nextInt();
            if (board[row - 1][col - 1] == '-') {
                board[row - 1][col - 1] = currentPlayer;
                return;
            }

        }

    }

    private static void switchPlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    private static boolean isWinner() {
        if (checkRow() || checkCol() || checkFirstDiagnol() || checkSecondDiagnol()) {
            return true;
        }
        return false;
    }

    private static void printWinner() {
        System.out.println(currentPlayer + " Win!!");
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (board[row - 1][i] != currentPlayer) {
                return false;
            }

        }
        return true;
    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (board[i][col - 1] != currentPlayer) {
                return false;
            }

        }
        return true;
    }

    private static boolean checkFirstDiagnol() {
        for (int i = 0; i < 3; i++) {
            if (board[0][0] != '-' && board[0][0] == board[1][1] && board[0][0] == board[2][2]) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkSecondDiagnol() {
        for (int i = 0; i < 3; i++) {
            if (board[0][2] != '-' && board[0][2] == board[1][1] && board[0][2] == board[2][0]) {
                return true;
            }
        }
        return false;
    }

    private static boolean isDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;

    }

    private static void printDraw() {
        System.out.println("This Turn Is Draw!!");
    }
}
